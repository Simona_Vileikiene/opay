# Laravel Opay
Opay 8.1 (https://opay.lt) module for Laravel 5.6.

## Installation
First require package with composer:
```sh
$ composer require beenet/laravel-opay
```
Add service provider to config/app.php:
```php
'providers' => [
    ...
    Beenet\Opay\OpayServiceProvider::class,
],
```
Publish config:
```sh
$ php artisan vendor:publish --provider="Beenet\Opay\OpayServiceProvider"
```


## Usage

Get payment methods
```php
$opayManager = new OpayManager();
$opayManager->getPaymentMethods();
```

## Prapare payment
Create router for client successfully payment
& router for payment callback (POST method) 
Don't forget to disabled this router in VerifyCsrfToken Middleware
```php
$opayManager = new OpayManager();
$opayManager->getPaymentPage($orderId, $total, $paymentOptions);
```

Get payment callback
```php
$opayManager = new OpayManager();
$opayManager->getPaymentCallback(Illuminate\Http\Request $request);
```
