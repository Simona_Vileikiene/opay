<?php
return [
    'website_id'     => 'website_id',
    'user_id' => 'user_id',
    'signature'      => 'signature',
    'redirect_url'          => 'http://beenet.lt/success',
    'web_service_url'       => 'http://beenet.lt/callback',
    'test' => TRUE,
];