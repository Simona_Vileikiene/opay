<?php

namespace Beenet\Opay\Facades;

use Illuminate\Support\Facades\Facade;

class Opay extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'opay';
    }
}
