<?php

namespace Beenet\Opay;

use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\URL;
use WebToPay;

class OpayManager
{

    private static $instance;

    /**
     * Create instant of self or return already created instant
     *
     * @param array|null $config
     */
    public static function getInstance($config = null)
    {
        return static::$instance ?: (static::$instance = new self($config));
    }


    /**
     * Prepare payment redirect page
     *
     * @param int $orderId
     * @param int $total
     * @param array $paymentOptions
     * @return string
     * @throws \Exception
     */
    public function getPaymentPage($orderId = 1, $total = 1, $paymentOptions = [])
    {
        $opay = new \OpayGateway();
        $opay->setSignaturePassword(config('opay.signature'));

        $paramsArray = $this->prepareParameters($orderId, $total, $paymentOptions);
        $paramsArray = $opay->signArrayOfParameters($paramsArray);
        try {
            return $opay->generatetAutoSubmitForm('https://gateway.opay.lt/pay/', $paramsArray);
        } catch (Exception $e) {
            throw new \Exception(get_class($e) . ': ' . $e->getMessage());
        }
    }

    /**
     * Prepare payment from
     * @param int $orderId
     * @param int $total
     * @param array $paymentOptions
     * @return array
     */
    public function getPaymentFrom($orderId = 1, $total = 1, $paymentOptions = [])
    {
        $opay = new \OpayGateway();
        $opay->setSignaturePassword(config('opay.signature'));
        $paramsArray = $this->prepareParameters($orderId, $total, $paymentOptions);
        $paramsArray = $opay->signArrayOfParameters($paramsArray);
        $sendEncoded = true;
        $str = '';
        if ($sendEncoded == true) {
            $encoded = $opay->convertArrayOfParametersToEncodedString($paramsArray);
            $str .= '<input type="hidden" name="encoded" value="' . $encoded . '" />';
        } else {
            foreach ($paramsArray as $key => $val) {
                if (!is_null($val)) {
                    if (is_bool($val)) {
                        $val = (int)$val;
                    }
                    $str .= '<input type="hidden"  name="' . htmlspecialchars($key, ENT_COMPAT, 'UTF-8') . '" value="' . htmlspecialchars($val, ENT_COMPAT, 'UTF-8') . '" />';
                }
            }
        }
        return [
            'link' => htmlspecialchars('https://gateway.opay.lt/pay/?', ENT_COMPAT, 'UTF-8'),
            'form' => $str
        ];
    }

    /**
     * Prepare payment callback
     * @param Request $request
     * @return array
     * @throws \OpayGatewayException
     */
    public function getPaymentCallback(Request $request)
    {
        $opay = new \OpayGateway();
        $opay->setSignaturePassword(config('opay.signature'));
        $parametersArray = $opay->convertEncodedStringToArrayOfParameters($request->input('encoded'));
        if (isset($parametersArray)) {
            if ($opay->verifySignature($parametersArray)) {
                return $parametersArray;
            } else {
                throw new \Exception('Payment error');
            }
        }
    }

    /**
     * Return available payment methods
     *
     * @return mixed
     * @throws \Exception
     */
    public function getPaymentMethods()
    {
        $opay = new \OpayGateway();
        $opay->setSignaturePassword(config('opay.signature'));
        $paramsArray = $this->prepareParameters();
        $paramsArray = $opay->signArrayOfParameters($paramsArray);
        $api = $opay->webServiceRequest('https://gateway.opay.lt/api/listchannels/', $paramsArray);
        if (!empty($api['response']['result'])) {
            return $api['response']['result'];
        } else {
            throw new \Exception($api['response']['errors'][0]['message']);
        }
    }

    /**
     * Prepare payment parameters
     *
     * @param int $orderId
     * @param int $total
     * @param array $payemntOptions
     * @return array
     */
    private function prepareParameters($orderId = 1, $total = 1, $payemntOptions = [])
    {
        $paramsArray = array(
            'website_id' => config('opay.website_id'),
            'order_nr' => $orderId,
            'redirect_url' => config('opay.redirect_url'),
            'web_service_url' => config('opay.web_service_url'),
            'standard' => 'opay_8.1',
            'country' => 'LT',
            'language' => 'LIT',
            'amount' => $total * 100,
            'currency' => 'EUR',
        );
        if (config('opay.test')) {
            $paramsArray['test'] = config('opay.user_id');
        }
        $paramsArray = array_merge($paramsArray, $payemntOptions);
        return $paramsArray;
    }
}