<?php

namespace Beenet\Opay;

use Illuminate\Support\ServiceProvider;

class OpayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('opay.php')
        ]);
    }

    /**
     * Register the application services.
     *
     *
     *
     * @return void
     */
    public function register()
    {
        require_once(__DIR__.'/../lib/opay_8.1.gateway.inc.php');

        $this->app->singleton('opay', function() {
            return new OpayManager();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['opay'];
    }
}
